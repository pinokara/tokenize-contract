const NasiToken = artifacts.require("NasiToken");
const USDT = artifacts.require("USDT");
const Farm = artifacts.require("Farm");


contract("Farm", accounts => {
  it("Withdraw ", async () => {
    const decimals = Math.pow(10, 6);
    const REWARD = 50 * decimals;
    const [nasi, usdt] = await Promise.all([
      NasiToken.new({ from: accounts[0] }),
      USDT.new({ from: accounts[0] }),
    ]);
    const farm = await Farm.new(
      nasi.address,
      usdt.address,
      accounts[10],
      { from: accounts[0] }
    );
    await nasi.addMinter(farm.address, { from: accounts[0] });
    const usdtAddress = await farm.usdtAddress();
    assert.equal(usdtAddress, usdt.address);

    await usdt.transfer(accounts[1], 1000 * decimals, { from: accounts[0]});

    const balanceAcc1 = await usdt.balanceOf(accounts[1]);
    assert.equal(balanceAcc1, 1000 * decimals);

    await usdt.approve(farm.address, 1000 * decimals, { from: accounts[1]});

    await farm.deposit(100 * decimals, { from: accounts[1] });

    const counter = await farm.poolMemberCounter();
    assert.equal(counter, 1);

    assert.equal(await farm.members(1), accounts[1]);

    await farm.stake({ from: accounts[0] });

    assert.equal(await nasi.balanceOf(accounts[1]), REWARD);
  })
});