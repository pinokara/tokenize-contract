const NasiToken = artifacts.require("NasiToken");

contract("NasiToken", accounts => {
  it("should appeare right name", () =>
    NasiToken.deployed()
      .then(instance => instance.name.call())
      .then(name => {
        assert.equal(name, 'Nasi', "name fail");
      })
  );
  it("should appeare right symbol", () =>
    NasiToken.deployed()
      .then(instance => instance.symbol.call())
      .then(symbol => {
        assert.equal(symbol, 'NASI', "symbol fail");
      })
  );

  it("should appeare right decimals", () =>
    NasiToken.deployed()
      .then(instance => instance.decimals.call())
      .then(decimals => {
        assert.equal(decimals, 6, "decimals fail");
      })
  );
});