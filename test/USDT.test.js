const USDT = artifacts.require("USDT");

contract("USDT", accounts => {
  it("should appeare right name", () =>
    USDT.deployed()
      .then(instance => instance.name.call())
      .then(name => {
        assert.equal(name, 'USDT', "name fail");
      })
  );
  it("should appeare right symbol", () =>
    USDT.deployed()
      .then(instance => instance.symbol.call())
      .then(symbol => {
        assert.equal(symbol, 'USDT', "symbol fail");
      })
  );

  it("should appeare right decimals", () =>
    USDT.deployed()
      .then(instance => instance.decimals.call())
      .then(decimals => {
        assert.equal(decimals, 6, "decimals fail");
      })
  );

  it("should appeare right decimals", () =>
    USDT.deployed()
      .then(instance => instance.totalSupply.call())
      .then(totalSupply => {
        assert.equal(totalSupply.toNumber(), 1000000 * Math.pow(10, 6), "total supply fail");
      })
  );
});