// SPDX-License-Identifier: MIT
pragma solidity ^0.5.13;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "./erc20/NasiToken.sol";

contract Farm is Ownable {
  using SafeMath for uint256;
  mapping (address => uint256) public balanceInPool;
  mapping (uint256 => address) public members;
  uint256 public poolMemberCounter = 0;
  uint256 public REWARD = 50 * 10**uint256(6);

  NasiToken internal nasiToken;
  address public nasiPool;
  address public usdtAddress;
  address public usdtPool;

  event Stake(address member, uint256 reward);

  constructor(
    address _nasiTokenAddress,
    address _usdtAddress,
    address _usdtPool
  ) public {
    nasiToken = NasiToken(_nasiTokenAddress);
    usdtAddress = _usdtAddress;
    usdtPool = _usdtPool;
  }

  function changeUSDTPoolAddress(address pool) external onlyOwner {
    require(pool != address(0), 'Pool can not equal Address 0');
    usdtPool = pool;
  }

  function changeUSDTAddress(address usdt) external onlyOwner {
    require(usdt != address(0), 'USDT can not equal Address 0');
    usdtAddress = usdt;
  }

  function deposit(uint256 amount) external {
    IERC20(usdtAddress).transferFrom(msg.sender, usdtPool, amount);
    balanceInPool[msg.sender] += amount;
    poolMemberCounter++;
    members[poolMemberCounter] = msg.sender;
  }

  function stake() external onlyOwner {
    require(poolMemberCounter > 0, '');
    for (uint256 i = 1; i <= poolMemberCounter; i++) {
      nasiToken.mint(members[i], REWARD.div(poolMemberCounter));
      emit Stake(members[i], REWARD.div(poolMemberCounter));
    }
  }
}