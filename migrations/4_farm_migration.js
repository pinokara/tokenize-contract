const fs = require('fs-extra')
const path = require('path')
const Farm = artifacts.require('Farm.sol');

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0];
    const farm = await deployer.deploy(
      Farm,
      process.env.tokenAddress,
      process.env.usdtAddress,
      '0x340841A5e1Fb401a4E039C9692cD8526e7bC9bc9',
      { from: adminAddress },
    );

    let outputValues = {
      adminAddress,
      tokenAddress: process.env.tokenAddress,
      usdtAddress: process.env.usdtAddress,
      farmAddress: farm.address,
    }
    const outputFilePath = path.join(__dirname, 'migration-output.json')
    fs.removeSync(outputFilePath)
    console.log(`Migration output values: ${JSON.stringify(outputValues)}`)
    fs.writeFile(outputFilePath, JSON.stringify(outputValues), (err) => {
      if (err != null) {
        console.log(err)
      }
    })
  })
}
