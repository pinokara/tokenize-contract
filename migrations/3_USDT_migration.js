const USDT = artifacts.require('USDT');
const contractConfig = require('../contract-config.js');

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0] || contractConfig[network].adminAddress;
    const usdt = await deployer.deploy(USDT, { from: adminAddress });
    
    process.env.usdtAddress = usdt.address;
  })
}
