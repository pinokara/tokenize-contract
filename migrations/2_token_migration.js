const contractConfig = require('../contract-config.js');
const NasiToken = artifacts.require('NasiToken');


module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0] || contractConfig[network].adminAddress;
    const token = await deployer.deploy(NasiToken, { from: adminAddress });
    process.env.tokenAddress = token.address;
  })
}
